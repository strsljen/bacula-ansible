# Bacula

This role will provision Bacula server, configure it and provision bacula
clients according to list provided.

All involved nodes are expected to be set properly: added to DNS with
proper DNS resolving, all filesystems prepared in advance, etc...

Ansible inventory has to be correct and all nodes accessible via Ansible.

For LVM related tasks, check out vg-lvm-fs-mount role.

## Description

Bacula is a proven open-source backup/archive solution which supports
full/incremental backup and retention policies.

Bacula is to be used for backup of directories/files on client server based on
custom rules. 

For some specific use-cases, Bacula supports pre- and post- backup scripts
which can prepare dbdump to be taken along.

## Bacula architecture

Bacula consists of five major components:
- Director
- Console
- Client
- Storage
- Catalog

### Bacula Director

Bacula Director supervises all the backup, restore and verify operations.

It is used to schedule backups and to recover files.

The Director runs as a daemon (or service) in the background.

### Bacula Client

Bacula Client service (also known as the File Daemon) is installed on each
machine to be backed up and runs as a daemon.

Bacula clients are available for variety of operating systems.

### Bacula Storage

Bacula Storage performs storing of the file attributes and data to the 
physical backup media or recovery of the file attributes and data and is 
responsible for reading and writing your tapes (or other storage media, e.g.
files).

It runs as a daemon on the machine that has the backup device
(for example a tape drive or a large disk).

### Catalog

Catalog services are comprised of the software programs responsible for
maintaining the file indexes and volume databases for all files backed up.
It permit the system administrator or user to quickly locate and restore any
desired file.

The Catalog services sets Bacula apart from simple archiver programs like tar
and dump, because the catalog maintains a record of all Volumes used, all Jobs
run, and all Files saved, permitting efficient restoration and Volume management.

Bacula can run with three SQL database backends: MySQL, PostgreSQL and SQLite.

We will use PostgreSQL in thius automation. 

### Dealing with firewalls

Since Bacula is a network backup solution, we need to consider opening following
ports:

```
Source: Console
Destination: Director
Port: 9101/TCP
```

```
Source: Director
Destination: Storage Daemon
Port: 9103/TCP
```

```
Source: Director
Destination: File Daemon
Port: 9102/TCP
```

```
Source: File Daemon
Destination: Storage Daemon
Port: 9103/TCP
```

In this setup we are runing Bacula Director and Storage Deamon both on Bacula
server.

In addition there is a File Daemon on Bacula server too in order to handle bacula
client part (Bacula server is also a managed node in Bacula)

Console also resides on Bacula server.

On Bacula clients there will be only Bacula File Daemon.

With that being said, in regards to firealls, we need to enable following:

```
Source: Bacula server
Destination: Bacula client
Port: 9102/TCP

Source: Bacula client
Destination: Bacula server
Port: 9103/TCP
```

### How Bacula works

The `job` is a most important term to understand. We can have backup or restore
jobs.

The `volume` is a place where bacula stores backed up data. In can be tape or
disk (file).

`Bacula backs up files.` Period. This is easy to understand since everything in
Linux is actualy file, in a way.

`Failed jobs.` If job is fails, we can restore already saved data from failed
job too.

#### Jobs explained step by step

As shown in figure below, this is what happens when running a backup job:

1. the Bacula Director will connect to the File Daemon and Storage Daemon

2. the Director will then send to the File Daemon the necessary information
to actually realize the backup job, like the Level, the File Set, the Storage
Daemon to contact, etc.

3. then the File Daemon will contact the Storage Daemon, identify the Data to
be sent and send them as well as the related meta data (file attributes) to the
Storage Daemon

4. the Storage Daemon will put them into one or more Bacula Volumes, according
to the Pool Resource chosen for the Job

5. the Storage Daemon will send metadata back to the Director.


Data stream: When running a backup job, the stream of data is sent from File
Daemon (FD) to Storage Daemon (SD), both data and metadata.

At the end of a backup job, SD sends metadata to Director to be written into
the Catalog.

Metadata is stored in SQL Catalog (in our case PostgreSQL database).

Catalog management is directly related to keeping backup Jobs data available,
and it is often automatically triggered by finishing Jobs

If we lose Catalog, we lost it all. No data can be recovered without metadata
stored in Catalog. Keep Catalog and it's backups safe!

### Full / incremental / differential backups

In order to successfully manage Bacula backup infrastructure, we need to
understand 3 types of backups present:

#### Full backups

Full backups secure the complete defined data set (called FileSet in Bacula’s
configuration files).

They consume more resources for their execution, both in storage, network,
bandwidth and execution times, and are the most complete and fastest to restore
from in the event of disaster.

#### Incremental backups

Incremental backups are all the changed files since the last backup (no matter
which level). 

They typically have smaller execution times and consume less physical and logical
resources than the entire file sets that make up a full backup.

Over time the logical management of a large series of incremental file backup
can slow down restore times.

But that is how incremental backup software works on Windows, Linux and other OSes.

#### Differential backups

Differential backups contain all combined file changes since the last full.

Differential backups have the potential to decrease restore times a lot, because
for a given point in time recovery you do not need the last full plus all
incremental backups.

You can use one full, one differential and perhaps some incremental backups that
have been running after the last differential.

In addition to being a support element for data backup and restoration operations,
it also contributes to time and cost savings. The number of volumes that Bacula
needs to access on disk, or the number of tapes that need to be read in for the
restore operations will be significantly smaller. Differential backup software is
convenient and fast.

#### Typical Backup schedule

F = full backup

D = differential backup

I = incremental backup

Week Sun Mon Tue Wed Thu Fri Sat
1    F   I   I   I   I   I   I
2    D   I   I   I   I   I   I
3    D   I   I   I   I   I   I
4    D   I   I   I   I   I   I
5    F   I   I   I   I   I   I

Basically, we run full backups once per month, differential once per week and
incremental in between.

This schedule has to be closely in sync with retention periods.

Example in Bacula conf file:

```
Schedule {
  Name = "SchedWeeklyCycle"
  Run = Full 1st sun at 23:05
  Run = Differential 2nd-5th sun at 23:05
  Run = Incremental mon-sat at 23:05
}
```

### Retention periods

It is crucial to understand retention periods in order to master Bacula backup
solution.

Bacula uses 3 kinds of retention periods:

- File Retention Period
- Job Retention Period
- Volume Retention Period

#### File Retention Period

The File Retention Period determines the time that File records are kept in the
Catalog database.

This period is important for two reasons: the first is that as long as File
records remain in the database, you can “browse” the database with a console
program and restore any individual file. Once the File records are removed or
pruned from the database, the individual files of a backup Job can no longer be
“browsed”.

The second reason for carefully choosing the File Retention Period is because
the database File records typically use the most storage space in the database.

As a consequence, you must ensure that regular “pruning” of the database file
records is done to keep your database from growing too big.

#### Job Retention Period

The Job Retention Period is the length of time that Job records will be kept in
the database.

Note, all the File records are tied to the Job that saved those files.

The File records can be purged, leaving the Job records. In this case, information
will be available about the Jobs that ran, but not the details of the files that
were backed up. When pruning a Job, Bacula will purge all its File records.

#### Volume Retention Period

The Volume Retention Period is the minimum time following the last write that a
Volume will be kept until the Volume can be reused.

Bacula will normally not overwrite a Volume that contains data still inside its
Retention period, but if Bacula runs out of usable Volumes, it can select any
Volume which is out of its Retention time for recycling, at this time automatically
removing the related Job and File information from the Catalog.

#### Pruning

Pruning is a process of removing information from the Catalog which match
retention period as expired.

Bacula by default automatically prunes the catalog database entries according to
the retention periods defined.

As a Bacula volume can contain one or more jobs (or parts of jobs) and a job
contains one or more files, the pruning process will have side effects:

- when pruning a Volume, all the jobs related to the Volume are pruned
- when pruning a Job, all the files related to this jobs are pruned

That is the reason why you should have the following:

    Rf <= Rj <= Rv

where:

Rf = File retention period

Rj = Job retention period

Rv = Volume retention period

#### Purging

Once all the database records that concern a particular Volume have been “pruned”
as described above respecting the retention periods, the Volume is said to be
“purged” (i.e. has no more catalog entries).

It is, however, possible to submit commands to Bacula to purge specific information,
which will not respect configured retention periods. Naturally, this is something
that should only be done with the greatest care.

### Volumes

Bacula manages all its storage compartments – Volumes in our terminology – in
Pools.

In particular, a Job is not configured to write to any particular Volume, but
to a set of Volumes called a Pool.

Volumes of Bacula are always members of only one Pool. There can however be
multiple pools.

#### The Pools

Pools in Bacula are just a way to manage volumes and to manage collections of
similar volumes.

There can be many reasons to use or not use several pools; the Bacula configuration
gives the Backup Administrator the ability to match the defined requirements.

Due to how Bacula manages Volume Retention periods, the jobs put into the same
pool should have the same retention periods.

Pool types:

##### Scratch pool

A Scratch pool contains volumes that may be with any pool.

If no volumes are available for a particular pool Bacula will look in the Scratch
pool and move the volume to the pool in question.

##### Backup pool

A Backup pool contains volumes intended to keep data from backups for a defined
retention period.

It can also include Recycled or Purged volumes, i. e. Volumes that are eligible
to be overwritten.

##### Recycle pool

A Recycle pool contains volumes after the purging process freed them, after they
have been used in a Backup pool.

# Supported OS

The role is written for Debian Linux. If used on other Linux flavors, it needs to be checked
and updated, if needed.

Almost certainly, some changes will be needed :)

# Defaults

    bacula_db_user
    bacula_db_password

Bacula database user and password

    bacula_shell_user
    bacula_shell_group

Bacula shell user and group

    bacula_db_address

Bacula database server address (in out case a localhost)

    bacula_db_name

Bacula database name

    bacula_basedir

Bacula base directory. Basically a main mount point under which some other reside.
Recommended to be a separate filesystem under LVM.

    bacula_datadir_backup

Bacula backup directory. By default sitting under bacula base directory.

    bacula_datadir_restore

Bacula restore directory. By default sitting under bacula base directory.

    bacula_confd

Bacula conf directory. Usually sitting in /etc/bacula

    bacula_scripts_dir

Bacula scripts directory. Contains various scripts, like pre- and postbackup
scripts for various clients, for example.

    bacula_sqldumpdir

Directory used for sql dbdump for Bacula Catalog

    bacula_catalog

Bacula Catalog name. Has to be overriden!

    bacula_director_name

Bacula director name. Has to be overriden!

    bacula_prebackup_script
    bacula_postbackup_script

Defualt location for bacula pre- and postbackup scripts

    bacula_dir_server_password
    bacula_monitor_password
    bacula_fd_server_password
    bacula_sd_server_password

Bacula secrets for Director, Monitor, StorageDaemon and FileDaemon.

Absolutelly have to be overriden!

In addition: beware of password length. Length 12 works ok, while
length 16 can cause issues.

Consult documentation for recommendations.

# Variables

    bacula_pkgs

List of Bacula packages to be installed.

    bacula_file_retention
    bacula_job_retention
    bacula_max_concur_jobs
    bacula_volume_retention_full
    bacula_volume_retention_inc
    bacula_volume_retention_diff
    bacula_volume_use_duration_full
    bacula_volume_use_duration_inc
    bacula_volume_use_duration_diff
    bacula_max_volumes_full
    bacula_max_volumes_inc
    bacula_max_volumes_diff

Bacula retention/volume_use/volume_number definitions.

Bacula documentation to be consulted before messing with them.

# Bacula client variables

All client variables are defined in dictionary: `bacula_client_list`.

Dictionary consists of:

    bacula_client_name

Name of the bacula client

    bacula_client_password

Password for Bacula File Daemon for the client

    bacula_client_catalog

Name of the Bacula Catalog (has to match with existing Catalog name on the Bacula Server)

    bacula_client_file_retention

Retention policy for the client files

    bacula_client_job_retention

Retention policy for the client jobs

    bacula_client_autoprune

Auto-prune option

    bacula_client_prebackupscript

Bacula client prebackup script (ClientRunBeforeJob)

Script to be run before the backup (if this member is missing, this option will
not be configured)

Script to be run by Bacula before backup job. Bacula client is running as root
user. If we need to run script as some other user we need to use 'su' command.
(example shown below)

    bacula_client_prebackupscript_template

Prebackup script template file.

We decided to use common prebackup script location on target server.
However, we need to point where Ansible should take script template from to
push it to managed server.

    bacula_client_postbackupscript

Bacula client postbackup script (ClientRunAfterJob)

Script to be run by Bacula after backup job is done. Sometimes we need some
cleaning to do as part of a backup job. This is the way to do it.

Bacula client is running as root user. If we need to run script as some other
user we need to use 'su' command. (example shown below)

    bacula_client_postbackupscript_template

Postbackup script template file.

We decided to use common postbackup script location on target server.

However, we need to point where Ansible should take script template from to
push it to managed server.

    bacula_client_fileset

File Set to be used (has to be defined on Bacula Server)

    bacula_client_fileset_list

In case that default "Full Set" is not used, there has to be included a list
with directories to be backed-up.

    bacula_client_fileset_list_exclude

Exclude list with directories to be excluded

    bacula_client_present

A boolean specifying if the client config should be added to the Bacula Server
or not.

This is normally set to `True`, but should be set to `False` when you want to
remove the config from the Bacula server. When removing a config it is enough to
set the `bacula_client_name` and `bacula_client_present` variables.

This allows us to control both adding and removal of Bacula clients through
Ansible.

Removal of clients is limited to only removing configuration on Bacula server
side. No packages are removed, nor bacula-fd stopped on client side for the
time being.

In addition, we keep configuration parameters in Ansible, along with current
backups for a removed client on a server which gives us failsafe option and
easy way to put it back to where we left if we decide at some point.

# Real life role example

One simple example of variables needed for provisioning Bacula server and some
clients with one plain and one with pre- and postbackup scripts.

Note: postgres dbdump pre- and postbackup script is included in this repo and
can be used, or taken as simple example.



```
bacula_db_password: "{{ vault_bacula_db_password }}"
bacula_dir_server_password: "{{ vault_bacula_dir_server_password }}"
bacula_monitor_password: "{{ vault_bacula_monitor_password }}"
bacula_fd_server_password: "{{ vault_bacula_fd_server_password }}"
bacula_sd_server_password: "{{ vault_bacula_sd_server_password }}"

bacula_director_name: "backup"
bacula_catalog: "Yggdrasil"

# Bacula list of clients
bacula_client_list:
  proxy:
    bacula_client_name: "proxy"
    bacula_client_password: "{{ vault_client_proxy_password }}"
    bacula_client_catalog: "{{ bacula_catalog }}"
    bacula_client_file_retention: "30 days"
    bacula_client_job_retention: "1 month"
    bacula_client_autoprune: "yes"
    bacula_client_fileset: "fileset-proxy"
    bacula_client_fileset_list:
      - "/etc"
    bacula_client_present: True
  monitoring:
    bacula_client_name: "monitoring"
    bacula_client_password: "{{ vault_client_monitoring_password }}"
    bacula_client_catalog: "{{ bacula_catalog }}"
    bacula_client_file_retention: "30 days"
    bacula_client_job_retention: "1 month"
    bacula_client_autoprune: "yes"
    bacula_client_prebackupscript: "su - postgres -c \\\"{{ bacula_prebackup_script }}\\\""
    bacula_client_prebackupscript_template: "etc/bacula/scripts/postgresql_prebckp.j2"
    bacula_client_postbackupscript: "su - postgres -c \\\"{{ bacula_postbackup_script }}\\\""
    bacula_client_postbackupscript_template: "etc/bacula/scripts/postgresql_postbckp.j2"
    bacula_client_fileset: "fileset-monitoring"
    bacula_client_fileset_list:
      - "/etc"
      - "/var/lib/postgresql/backups"
    bacula_client_present: True
```
